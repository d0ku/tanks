#include "./libs/stdio.h"
#include "./libs/conio.h" //collides with ncurses
#include "./libs/poll.h"
#include "./libs/time.h" // 20:10 15:15 10:20 // nieprzejezdna woda //do poprawy kolizje  przy hop >1
#include "./libs/stdlib.h"


	//start of configuration part
 		//first shoot is added, then the player moves, but first player moves, second shot moves
#define  	round_count 		2500 // defines how many rounds lasts the game 
#define  	map_size_x 			50 // size of axis x
#define  	map_size_y 			50 // size of axis y
#define  	number_of_bullets 	1000 // number of bullets in game
#define 	hop					1  //defines how many dots per move moves bullet
#define     start_x_1			10 // defines starting x position of player 1
#define     start_y_1           10 // defines starting y postion of player 1
#define     start_x_2           20 // defines starting x position of player 2
#define     start_y_2           10 // defines starting y position of player 2
#define     start_lifes_1		3 // defines how many hearts does player 1 starts with
#define     start_lifes_2       3 // defines how many hearts does player 2 starts with
#define 	delay 				25 // delay beetween moves
	//end of configuration
int 	player_1_icon='X';
char 	player_2_icon='W';
char 	bullet_directory_player_1='d'; // check directory of shooting for player 1
char 	live_char = 'V'; //this is symbol for lives 
char 	no_live   = 'U'; //this is symbol for lost live
char 	bullet_directory_player_2='a'; // check directory of shooting for player 2
char 	whether_end;
char 	undestructable_block='#';
char  	destructable_block='@';
char 	hiding_block='H';
char 	half_destructed_block='$';
char 	normal_block='.';

int 	times=0;
int 	level=1 ; //how hard is the game , 1-easy, 2 -medium, 3-hard
int 	actual_y;
int 	actual_x;
int 	how_long=1; //decides which level to load


char 	bullet_directory[number_of_bullets]; 	// number of bullets, coordinates in y and x axis , directory of bullet ( a, d , w , s)
int 	bullet_x[number_of_bullets]; 			//
int 	bullet_y[number_of_bullets]; 			//
int 	bullet_number=0;
int 	player_1_lifes=start_lifes_1; //player 1 lifes   
int 	player_1_x = start_x_1; //player 1 x coordinate
int 	player_1_y = start_y_1;  //player 1 y coordinate
int 	player_2_lifes=start_lifes_2; //player 2 lifes
int 	player_2_x=start_x_2;
int 	player_2_y=start_y_2;

void logos (int number)
{
		system("clear");
	switch (number)
	{

			case 1:
			{

	
	printf("####################################################################\n");
	printf("#     XXXXXXXXXX         X          X     X     X   X     XXXXX    #\n");
	printf("#         XX            X X         XX    X     X  X      X        #\n");
	printf("#         XX           X   X        X X	  X     X X       X        #\n");
	printf("#         XX          X     X       X  X  X     XX        XXXXX	   #\n");
	printf("#         XX         XXXXXXXXX      X   X X     X X           X	   #\n");
	printf("#         XX        X         X     X    XX     X  X          X	   #\n");
	printf("#         XX       X           X    X     X     X   X     XXXXX	   #\n");
	printf("#                                                                  #\n");
	printf("#     Push any button to play ( h for help)(o for options)         #\n");
	printf("####################################################################\n");

			} break;
			case 2: 
			{
	printf("####################################################################\n");
	printf("#                                                                  #\n");
	printf("#   HOW TO MOVE                           HOW TO SHOT              #\n");
	printf("#                                                                  #\n");
	printf("#        up              *********        ###############          #\n");
	printf("#        W               *bullets*        #    SPACE    #          #\n");
	printf("# left A   D  right      *********        ###############          #\n");
	printf("#        S                                                         #\n");
	printf("#       down                                                       #\n");
	printf("#                                                                  #\n");
	printf("#       %c   your lives             your lost lives %c               #\n",live_char,no_live);
	printf("#   %c =this is you            #=you can't destroy that block...    #\n",player_1_icon);
	printf("#   %c =this is your enemy     $,@= but you can destroy those       #\n",player_2_icon);
	printf("#                                                                  #\n");
	printf("#                         Push any button to play                  #\n");
	printf("####################################################################\n");


			} break;

			case 3:
			{

	printf("####################################################################\n");
	printf("#                                                                  #\n");
	printf("#     X   X   XXX   X   X         x       x   XXX   x   x          #\n");
	printf("#      X X   x   x  X   x         x   x   x  x   x  XX  x          #\n");
	printf("#       X    X   X  X   X         x   x   x  x   x  x x x          #\n");
	printf("#       X    x   x  x   x         x   x   x  x   x  x  XX          #\n");
	printf("#       x     XXX    XXX           XXX XXX    xxx   x   x          #\n");
	printf("#                                                                  #\n");
	printf("#                                                                  #\n");
	printf("#                                                                  #\n");
	printf("#                        I tak umrzesz 3:)                         #\n");
	printf("#                      You'll die anyway 3:)                       #\n");
	printf("#                                                                  #\n");
	printf("#           Push any button to keep playing (q to quit)            #\n");
	printf("####################################################################\n");

		
			} break;

			case 4:
			{
	printf("####################################################################\n");
	printf("#                                                                  #\n");
	printf("#                                                                  #\n");
	printf("#     X   X   XXX   X   X         x       x   XXX   x   x          #\n");
	printf("#      X X   x   x  X   x         x   x   x  x   x  XX  x          #\n");
	printf("#       X    X   X  X   X         x   x   x  x   x  x x x          #\n");
	printf("#       X    x   x  x   x         x   x   x  x   x  x  XX          #\n");
	printf("#       x     XXX    XXX           XXX XXX    xxx   x   x          #\n");
	printf("#                                                                  #\n");
	printf("#                                                                  #\n");
	printf("#                 I tak umrzesz 3:)  (kurde, jednak nie...)        #\n");
	printf("#               You'll die anyway 3:) (shieet, maybe not...)       #\n");
	printf("#                                                                  #\n");
	printf("#   Push any button to restart the game (q to quit)(r to rank)     #\n");
	printf("####################################################################\n");
			} break;

			case 5:
			{

	printf("####################################################################\n");
	printf("#                                                                  #\n");
	printf("#                                                                  #\n");
	printf("#    X   X   XXX   X   X       X        XXX     XXXX    XXXXX      #\n");
	printf("#     X X   X   X  X   X       X       X   X   x          x        #\n");
	printf("#      X    X   X  X   X       X       X   X    XXXX      x        #\n");
	printf("#      X    x   x  x   x       x       X   x        x     x        #\n");
	printf("#      x     xxx    xxx        xxxxx    xxx     XXXX      x        #\n");
	printf("#                                                                  #\n");
	printf("#                                                                  #\n");
	printf("#                        I tak umrzesz 3:)  (kurde, jednak nie...) #\n");
	printf("#                      You'll die anyway 3:) (shieet, maybe not...)#\n");
	printf("#                                                                  #\n");
	printf("# Push any button to restart the game (q to quit)(r to rank)       #\n");
	printf("####################################################################\n");



			} break;

			case 6:
			{
	printf("####################################################################\n");
	printf("#                                                                  #\n");
	printf("#                                                                  #\n");
	printf("#                  XXX      X       X   X   XXXX    XXXX           #\n");
	printf("#                 X   X    X X      X   X  X       X               #\n");
	printf("#                 XXXX    XXXXX     X   X  XXXXX   XXXXX           #\n");
	printf("#                 X      X     X    X   X      X   X               #\n");
	printf("#                 X     X       X   XXXXX  XXXX     XXXX           #\n");
	printf("#                                                                  #\n");
	printf("#                                                                  #\n");
	printf("#                   Push any button to continue                    #\n");
	printf("#                      (h) for help                                #\n");
	printf("#                      (q) to exit                                 #\n");
	printf("#                                                                  #\n");
	printf("####################################################################\n");





			} break;

	}



}
void restart()
{
	bullet_number=0;
	player_1_lifes=start_lifes_1; //player 1 lifes   
	player_1_x = start_x_1; //player 1 x coordinate
	player_1_y = start_y_1;  //player 1 y coordinate
	player_2_lifes=start_lifes_2; //player 2 lifes
	player_2_x=start_x_2;
	player_2_y=start_y_2;

	for (int i=0;i<number_of_bullets;i++)
	{
	bullet_x[i]=0;
	bullet_y[i]=0;
	}
	times=0;
}

void shot_adding(char directory_of_bullet, int player_y , int player_x) //responsible for adding shots to move table
{
	switch (directory_of_bullet) //x and y are coordinates of bullet, velocity is 2 units per move
			{

			case 'a': //maleje x, y bez zmian 
				{
				bullet_directory[bullet_number] = 'a';
				bullet_y[bullet_number]=player_y;
				bullet_x[bullet_number]= player_x-1;
				bullet_number++;
				} break;

			case 'd': //rośnie x, y bez zmian
				{
				bullet_directory[bullet_number] = 'd';
				bullet_y[bullet_number]=player_y;
				bullet_x[bullet_number]= player_x+1;
				bullet_number++;
				} break;

			case 'w': // maleje y, x bez zmian
				{
				bullet_directory[bullet_number] = 'w';
				bullet_y[bullet_number]=player_y-1;
				bullet_x[bullet_number]= player_x;
				bullet_number++;
				} break;


			case 's': // rośnie y, x bez zmian
				{
				bullet_directory[bullet_number] = 's';
				bullet_y[bullet_number]=player_y+1;
				bullet_x[bullet_number]= player_x;
				bullet_number++;
				} break;
			}	
			if (bullet_number>=number_of_bullets-1)
				bullet_number=0;
}

void map_load_file()	//initial loading of map and saving to map_tmp.txt
{

	FILE *map_original; 


	if (how_long==1) map_original=fopen("./maps/map_default.txt","r");
	if (how_long==2) map_original=fopen("./maps/map_level_2.txt","r");
	if (how_long==3) map_original=fopen("./maps/map_level_3.txt","r");
	if (how_long==4) map_original=fopen("./maps/map_level_4.txt","r");
	if (how_long==5) map_original=fopen("./maps/map_level_5.txt","r");
	if (how_long==6) map_original=fopen("./maps/map_level_6.txt","r");
	if (how_long==7) map_original=fopen("./maps/map_level_7.txt","r");
	if (how_long==8) map_original=fopen("./maps/map_level_8.txt","r");
	if (how_long==9) map_original=fopen("./maps/map_level_9.txt","r");

	fscanf(map_original,"%d",&actual_x);

	fscanf(map_original,"%d",&actual_y); //problem

	FILE *map_tmp=fopen("map_tmp.txt","w+");
	char buff='z';
   

   fscanf(map_original,"%c",&buff);
    for (int i=0;i<actual_y;i++)
   {
   		for(int j=0;j<actual_x+1;j++)
   		{fscanf(map_original,"%c",&buff);
   		fprintf(map_tmp, "%c",buff );
   		}
   	
   }
   fclose(map_original);
   fclose(map_tmp);
}

int collision_checking() //checks whether move is possible or not and deletes initial location of player on map_tmp
{

	FILE *map_tmp=fopen("map_tmp.txt","r");
	
	char map [actual_y][actual_x]; //declaration array ma	

	for (int i=0;i<actual_y;i++)
		for (int j=0;j<actual_x+1;j++)
					fscanf(map_tmp,"%c",&map[i][j]);

				fclose(map_tmp);
				int b;


	for (b=0;b<bullet_number;b++) //bullets part //bullet number for better performance, check whether number of bullets does not work different
	{
		
		for (int i=0;i<actual_y;i++)
		{for (int j=0;j<actual_x+1;j++)
			{

				if (bullet_y[b]==i&&bullet_x[b]==j&&map[i][j]==undestructable_block)
				{
					bullet_y[b]=0;
					bullet_x[b]=0;
				}

				if (bullet_y[b]==i&&bullet_x[b]==j&&map[i][j]==destructable_block)
				{
					bullet_y[b]=0;
					bullet_x[b]=0;
					map[i][j]=half_destructed_block;
				}

				if (bullet_y[b]==i&&bullet_x[b]==j&&map[i][j]==half_destructed_block)
				{
					bullet_y[b]=0;
					bullet_x[b]=0;
					map[i][j]=normal_block;
				}
			}
		}

	}



	FILE* map_tmp_save=fopen("map_tmp.txt","w+"); //saving status of destructed blocks //deletes initial location of players
	 for (int i=0;i<actual_y;i++)
   {
   		for(int j=0;j<actual_x+1;j++)
   		{if(map[i][j]!='1' && map[i][j]!='2')
   		fprintf(map_tmp_save, "%c",map[i][j] );
   		if(map[i][j]=='1'||map[i][j]=='2')
   			fprintf(map_tmp_save, "%c",normal_block);

   		}
   	
   }
   fclose(map_tmp_save);




	for (int i=0;i<actual_y;i++)// tanks part
			for (int j=0;j<actual_x+1;j++)
			{
				if(player_1_y==player_2_y && player_1_x==player_2_x)
					return 1;
				 if(player_1_y==i && player_1_x==j && (map[i][j]==undestructable_block || map[i][j]==destructable_block || map[i][j]==half_destructed_block))
					return 1;
						if((player_1_y==i && player_1_x==j) && (map[i][j]==undestructable_block || map[i][j]==destructable_block || map[i][j]==half_destructed_block))
							return 1;
						if(player_2_y==i && player_2_x==j && (map[i][j]==undestructable_block || map[i][j]==destructable_block || map[i][j]==half_destructed_block))
					return 1;
						if((player_2_y==i && player_2_x==j) && (map[i][j]==undestructable_block || map[i][j]==destructable_block || map[i][j]==half_destructed_block))
							return 1;
			}		
			return 0;
}

void map_print()		// responsible for printing map "image"
{

	FILE *map_tmp=fopen("map_tmp.txt","r");
	
	char map [actual_y][actual_x]; //declaration array map

	for (int i=0;i<actual_y;i++)
		for (int j=0;j<actual_x+1;j++)
		{
					fscanf(map_tmp,"%c",&map[i][j]);
					if(map[i][j]=='1') 
					{player_1_y=i;
						player_1_x=j;
					}
					if(map[i][j]=='2') 
					{player_2_y=i;
						player_2_x=j;
					}

		}
				fclose(map_tmp);
			
			
	
	
	
	if(map[player_1_y][player_1_x]!=hiding_block) //checks whether that place is hiding block or not
	map[player_1_y][player_1_x]=player_1_icon; //first int is axis y, second int is axis x, this is location of player 1 on the map
	if(map[player_2_y][player_2_x]!=hiding_block)
	map[player_2_y][player_2_x]=player_2_icon; //first int is axis t, second int is axis x, this is ocation of player 2 on map

	for (int i=0;i<actual_x;i++) // declaring borders of the map
	{
	map[0][i]='#';
	map[actual_y-1][i]='#';
	}
	for (int i=0;i<actual_y;i++)
	{
	map[i][0]='#';
	map[i][actual_x-1]='#';	
	}

	


	for (int i=0;i<number_of_bullets;i++) //declaring bullets location on the map
	{
		map[bullet_y[i]][bullet_x[i]]='*';
	}

	map[0][0]='P';						//that part is reponsible for declaring actual lifes
	map[0][1]='1';

	for (int i=3;i<player_1_lifes;i--)
	{
		map[0][player_1_lifes+2]=no_live;
	}

	for (int i=2;i<player_1_lifes+2;i++)
	{
		map[0][i]=live_char;

	}			

	map[0][actual_x-1]='P';
	map[0][actual_x-2]='2';

	for (int i=0;i<player_2_lifes;i++) 
	{
		map[0][actual_x-3-i]=live_char;

	}						

	for (int i=3;i>player_2_lifes;i--)
	{

		map[0][actual_x-2-i]=no_live;
	}// end of life part

	system("clear"); //clears the console , this is stricte linux command, may be problematic in ports !!!!!!!!!!!!!!!!!!

	for (int i = 0; i <actual_y ; ++i) //printing image
	{
		for (int j=0;j<actual_x;j++) 
		printf("%c",map[i][j]); 

			 printf("\n");
	}
}

void interact() // responsible for making changes at button push
{
	char action;
	int temp=0;

	
	system ("/bin/stty raw"); //start of a time limit for a move
	struct pollfd ufds[1];      
    ufds[0].fd = 0;
    ufds[0].events = POLLIN;
    int rv;
    rv = poll(ufds, 1, 500); //specific timeout for a move
    if (rv==0)
    	{printf("Timeout\n");
    
	}
    else if(rv==-1)
    {	printf("error\n");
    getch();
	}
	else if (ufds[0].revents & POLLIN) // end of a time limit for a move
	{
		action=getch();		

	system ("/bin/stty cooked");
	switch(action)
	{
		case 'w': //W 
		{ if (player_1_y==1) 
			break;

			temp=player_1_y;
			temp--;
			player_1_y=temp;
			if(collision_checking()==1)
				player_1_y++;
		} break;

		case 's': //S 
		{ if(player_1_y==actual_y-2) // tutaj wstaw map size y -2
			break;

			temp=player_1_y;
			temp++;
			player_1_y=temp;
			if(collision_checking()==1)
				player_1_y--;


		} break;

		case 'a': //A
		{if(player_1_x==1)
			break;

			temp=player_1_x;
			temp--;
			player_1_x=temp;
			if(collision_checking()==1)
				player_1_x++;

		} break;

		case 'd': //D
		{if(player_1_x==actual_x-2) //tutaj wstaw map size x -2
			break;

			temp=player_1_x;
			temp++;
			player_1_x=temp;
				if(collision_checking()==1)
				player_1_x--;
		} break;

		case 'q':
		{player_1_lifes=0;

		}
		case ' ': //space, shot
		{
			shot_adding(bullet_directory_player_1, player_1_y, player_1_x);
		}	break;

		case 27:
		{
			logos(6);
			char temp;
			temp=getch();
			if (temp=='q')
			{
				player_1_lifes=0;
			}
			if (temp=='h')
			{
				logos(2);
			}
			getch();


		} break;
	}

	if (action!=' ')
		bullet_directory_player_1=action;
	}
	 system ("/bin/stty cooked");
}



int hop_check(int i)
{


switch (bullet_directory[i])
{
	case 'a':
	{
		if(bullet_x[i]-hop<0) return 1;
		else return 0;
	} break;

	case 'd':
	{
		if (bullet_x[i]+hop>actual_x-1) return 1;
		else return 0;
	} break;

	case 'w':
	{
		if (bullet_y[i]-hop<0) return 1;
		else return 0;

	} break;

	case 's':
	{
		if (bullet_y[i]+hop>actual_y-1) return 1;
		else return 0;
	} break;

	
}
	return 0;
}



void bullet_movement() //changes positions of bullets
{

	for (int i=0;i<number_of_bullets;i++)
	{
		switch(bullet_directory[i])
		{
			case 'a':
			{
				collision_checking();
				if (hop_check(i)!=1)				
				bullet_x[i]-=hop;
				

			} break;

			case 'd':
			{
				collision_checking();
				if (hop_check(i)!=1)		
				bullet_x[i]+=hop;
			} break;

			case 'w':
			{
				collision_checking();
				if (hop_check(i)!=1)		
				bullet_y[i]-=hop;
			} break;

			case 's':
			{	
				collision_checking();
				if (hop_check(i)!=1)		
				bullet_y[i]+=hop;

			} break;



		}


	}
}

int damage() // checks health status of player_1 and player_2
{
	for (int i=0;i<number_of_bullets;i++)
	{
		if (player_1_x==bullet_x[i] && player_1_y==bullet_y[i])
			{
				player_1_lifes--;
				bullet_y[i]=0;
				bullet_x[i]=0;
			}

		if (player_2_x==bullet_x[i] && player_2_y==bullet_y[i])
			{
				player_2_lifes--;
				bullet_y[i]=0;
				bullet_x[i]=0;

			}
		
	}
	
	
	if (player_2_lifes==0)
	{	
			logos(3);
		whether_end=getch();
		if(whether_end=='q')
			return 0;
		if(whether_end!='z')
			how_long++;
		if(how_long==10)
			{logos(4);
			whether_end=getch();
			}
			if(whether_end=='q')
				return 0;
			else how_long=1;

		restart();
		map_load_file();
			
		
	}
	else if (player_1_lifes==0)
	{
		logos(5);
		whether_end=getch();
		if(whether_end=='q')
			return 0;
		restart();
			
	}
	return 0;
}

void bullets_initialization() //makes all bullets sit in 0,0
{
	for (int i=0;i<number_of_bullets;i++)
	{
		bullet_y[i]=actual_y-1;
		bullet_x[i]=actual_x-1;
	}
}

void intro_logo() //displays logo and options shortcut
{
	char temp;
	logos(1);
	temp =getch();
	if (temp=='h')
	{	logos(2);
		getch();
	}
	if (temp=='o')
	{
		system("clear");
		printf("What do you want to change?   \n");
		getch();
		system("gedit main.c");

	} if (temp=='l')
	{
		scanf("%d",&how_long);
	}
}

void bullets_clearing()
{
	for (int b=0;b<bullet_number;b++)
	{


		if (bullet_x[b]<=1 && bullet_directory[b]=='a' ) //responsible for crashes with hop =2
		{bullet_y[b]=0;
		bullet_x[b]=0;
		}
		if (bullet_directory[b]=='d' &&(bullet_x[b]>=actual_x-hop || bullet_x[b]==0))
		{bullet_y[b]=0;
		bullet_x[b]=0;
		}
		if (bullet_directory[b]=='w'  &&bullet_y[b]<=1)
		{bullet_y[b]=0;
		bullet_x[b]=0;
		}
		if (bullet_directory[b]=='s' && (bullet_y[b]>=actual_y-hop || bullet_y[b]==0))
		{bullet_y[b]=0;
		bullet_x[b]=0;
		}			

	}
}


void player_2_SI_easy() // random moves and shots
{
	srand(time(NULL));
	int r = (rand()%5); 

	switch (r)
	{
		case 0: // a
		{
			if(player_2_x==1)
				break;
			player_2_x--;
			bullet_directory_player_2='a';
				if(collision_checking()==1)
				player_2_x++;

		}break;

		case 1: //d
		{if(player_2_x==actual_x-2) //tutaj wstaw map size x -2
			break;
			player_2_x++;
			bullet_directory_player_2='d';
			if(collision_checking()==1)
				player_2_x--;
		}break;

		case 2: //w
		{
			if (player_2_y==1) 
				break;
			player_2_y--;
			bullet_directory_player_2='w';
			if(collision_checking()==1)
				player_2_y++;


		}break;

		case 3: //s
		{
			if(player_2_y==actual_y-2) // tutaj wstaw map size y -2
				break;
			player_2_y++;
			bullet_directory_player_2='s';
			if(collision_checking()==1)
				player_2_y--;
			

		}break;

		case 4: //space
		{
			shot_adding(bullet_directory_player_2,player_2_y,player_2_x);

		}break;
	}
}


int main()
{
	restart(); //initialiaze all settings

	bullets_initialization();

	intro_logo();

	map_load_file();

for (times=0;times<round_count;times++) //the game itself
	{

	map_print();

	bullets_clearing();

	printf("%d, %d, %d, %d \n",bullet_number, bullet_x[bullet_number-1],bullet_y[bullet_number-1], bullet_directory[bullet_number-1] );

	interact();
	map_print();
	
	bullet_movement();
	
	damage();

	if (whether_end=='q')
		{printf("Milego dnia, wroc jeszcze :) \n");
		return 0;
		}
		
	if (level==1)
			player_2_SI_easy();	

	}

map_print();

return 0;
}
